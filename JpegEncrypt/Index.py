from sys import argv
from binascii import hexlify
from optparse import OptionParser

def da(Start,End,data,none,size):
    found = 0
    while (0 == found) :
      Start += 2
      End += 2
      tmp = data[Start:End]
      if("ff" == tmp):
            Start+=2
            End+=2
            tmp = data[Start:End]
            if("d9" == tmp or tmp in size):
               found = 1
    return [Start, End]

def findFFD9(data):
    size = ["c0","c2","c4","db","da","fe","e0","e1","e2","e3","e4","e5","e6","e7","e8","e9","ea","eb","ec","ed","ee","ef","dd"]
    none = ["d8","d9","d0","d1","d2","d3","d4","d5","d6","d7"]
    Start = 0
    End = 2
    SeqSize = 0

    while(True):
        tmp = data[Start:End]
        if("ff" == tmp):
            Start +=2
            End +=2
        if(tmp in size):
            if ("da" == tmp):
                Start,End = da(Start, End, data, none, size)
            else:
                Start += 2
                End +=2
                SeqSize = data[Start:End+2]
                Start+=int(SeqSize,16)*2
                End +=int(SeqSize,16)*2

        elif(tmp in none):
            if("d9" == tmp):
                return [Start,End]
            else:
                Start += 2
                End += 2

def Encrypt(Input,Output):
    File =   open (Input,"rb")#Opening Source Image
    FileWrite = open(Output,"wb")#Opening Dest

    text = raw_input("Enter what to encrypt: ")
    data = File.read() + text
    FileWrite.write(data)

def Decrypt(Input,Output):
    File =   open (Input,"rb")
    FileWrite = open(Output,"wb")
    data =hexlify(File.read())
    Start,End = findFFD9(data)
    text = data[End:]
    FileWrite.write(text.decode("hex"))

def rotatechar(string):
   tmp = ""

   for i in range(0,len(string)+1):
       tmp += string[-i]

   return tmp

def main():
    parser = OptionParser()
    choose = 3

    parser.add_option("-O","--Options",action=  "store",dest = "choose",type = "int",help = "1 - Encrypt, 2 - Decrypt, 0 - Exit")
    parser.add_option("-D","--Dest",action = "store",dest = "EncryptDest",metavar = "FILE",help = "Destntion Image to encrypt")
    parser.add_option("-S","--SRC",action = "store" ,dest = "EncryptSRC",metavar = "FIlE", help = "Source Image to encryprt/decrypt")
    parser.add_option("-T","--Text",action = "store",dest = "Text",metaver = "FILE" , help = "File to write the Encrypted text into it .txt")
    if( "1" == choose):
        Encrypt(EncryptSRC,EncryptDest)

    elif("2" == choose):
        Decrypt(EncryptSRC,Text)

    elif("0" == choose):
        pass

    else:
        print "Error: Incorrect number"
        main()

if __name__ == '__main__':
    main()