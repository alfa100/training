#include <string.h>
#include <stdlib.h>
#include <stdio.h>


char * reverse_words(const char * str)
{
	char * retArray = NULL;
	int indexOf_retArray = 0;
	int lengthOfStr = 0;
	int StartingIndex = 0;
	int EndingIndex = 0;

	// Size of Str
	lengthOfStr = strlen(str);
	EndingIndex = lengthOfStr;
	// Setting up memory to Var
	retArray = (char*)malloc(lengthOfStr + 1);
	if (NULL == retArray)
	{
		goto Free;
	}

	//Setting up zero to all the Var
	memset(retArray, 0, lengthOfStr + 1);


	for (; EndingIndex > -1; EndingIndex--)
	{
		// Checking when the word end
		for (StartingIndex = EndingIndex - 1; str[StartingIndex] != ' ' && StartingIndex > -1; StartingIndex--)
		{
		}

		//Adding one because when we want the string without the space
		StartingIndex++;

		//Copyimg the word into the string
		memcpy(retArray + indexOf_retArray, str + StartingIndex, EndingIndex - StartingIndex);

		//Checking if we got to the end,So we dont need to add a space,but if not we add a space and setting up the Vars
		if (StartingIndex != 0)
		{
			//Adding a space
			retArray[strlen(retArray)] = ' ';
			retArray[strlen(retArray)] = '\0';

			//Addding to tmp
			indexOf_retArray += EndingIndex - StartingIndex + 1;
			EndingIndex = StartingIndex;
		}
	}

	//Returning the new string
	return retArray; 

Free:
	return NULL;
}

int main(void)
{
	const char str[] = "Tal Max Hason hello";
	char *ret = NULL;
	ret = reverse_words(str);
	if (NULL != ret)
	{
		printf("%s",ret);
		free(ret);
	}

	return 0;	
}


