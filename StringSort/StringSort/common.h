#pragma once
typedef enum _STATUS {
	STATUS_ERROR = -1,
	STATUS_SUCCESS = 0,



	STATUS_STRING_TO_WORDS_MALLOC1_FAILxbED,
	STATUS_STRING_TO_WORDS_MALLOC2_FAILED,
	STATUS_WORDS_TO_STRING_MALLOC1_FAILED,

	STATUS_DUPLICATE_BUFFER_MALLOC1_FAILED = 1000,

	STATUS_CHECKGET_FAILED = 2000,
	STATUS_TWO_STRINGS_TO_ONE_MALLOC1_FAILED,

	STATUS_SET_MSG_MALLOC1_FAILED = 3000,
	STATUS_SET_MSG_MALLOC2_FAILED,
	STATUS_START_HTTP_SERVER_MALLOC1_FAILED,
	STATUS_CREATE_DIR_MALLOC1_FAILED,
	STATUS_SET_MSG_OPEN_FAILED,
	STATUS_REPLAE_MALLOC1_FAILED,

} STATUS;


#define FREEVAR(a) \
	if(NULL != a)\
	{\
			free(a); \
			a = NULL;\
	}

//Copy buffer from one to other by start and length
STATUS DuplicateBuffer(const char * string, int Start, int length, const char ** BufferPointer);
STATUS replace(char * stringSrc, char ** StringDst);
STATUS TwoStringsToOne(const char * src1, const char * src2, char ** dst);

//A Func that sort pointer array
void BubbleSort(const char **const Words, int NumberOfWords);