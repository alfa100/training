#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/*
void strcut(char * WordString,int WordSize)
{
	char *tmp = NULL; 
	int lengthWordString = 0;

	lengthWordString = strlen(WordString);
	tmp = (char*)malloc(lengthWordString+1);
	memset(tmp, 0, strlen(tmp));
	memcpy(tmp, WordString, lengthWordString);
	memset(WordString, 0, strlen(WordString) + 1);
	memcpy(WordString, tmp, lengthWordString-WordSize);

	free(tmp);
}*/

typedef enum _STATUS {
	STATUS_ERROR = -1,
	STATUS_SUCCESS = 0,
	
	STATUS_STRING_TO_WORDS_MALLOC1_FAILED,
	STATUS_STRING_TO_WORDS_MALLOC2_FAILED,
} STATUS;

// Function that Get pointer to char and return how many words
int HowManyWords(char * WordString)
{
	int CountWords = 0;
	unsigned int  i = 0;
	while (i < strlen(WordString))
	{
		// A word is found by a space between 
		if (' ' == WordString[i])
		{
			// Add one to word
			CountWords++;
		}

		i++;
	}

	// Add one because that last one doesn't count in the while
	CountWords++;
	return CountWords;
}

int LengthOfString(char ** WordString,int NumberOfWords)
{
	int retlength = 0;

	retlength += NumberOfWords;
	NumberOfWords--;

	while (NumberOfWords >-1)
	{
		retlength += strlen(WordString[NumberOfWords]);
	}


	return retlength;
}
// A Func that gets a char pointer ,number of words And length of the string and return a pointer to words
STATUS StringToWords(char * WordString,int * NumberOfWords,char *** Words)
{
	char * word = NULL;
	int WordSize = 0;
	int  i = 0;
	int WordCount = 0;
	int lengthOfString = 0;
	int tmp = HowManyWords(WordString);
	lengthOfString = strlen(WordString);

	NumberOfWords = (int*)malloc(tmp);
	*NumberOfWords = tmp;
	// Alloctaing memory to Words
	*Words = (char**)malloc(*NumberOfWords * sizeof(char**));
	if (NULL == *Words)
	{
		return STATUS_STRING_TO_WORDS_MALLOC1_FAILED;
	}
	// Setting all the values to 0
	memset(Words, 0, *NumberOfWords * sizeof(char*));
	// I is the index of the array he goes on every char and setting up where is a word
	i = lengthOfString;

	while (i > -1)
	{

		// Space is the indictor for a Word and i==0 is because the last word doesn't Count
		if ((' ' == WordString[i]) ||( 0 == i) )
		{
			if (i !=0)
			{
				i++;
			}
			WordSize = lengthOfString - i;
 			word = (char*)malloc(WordSize);
			if (NULL == word)
			{
				return STATUS_STRING_TO_WORDS_MALLOC2_FAILED;
			}
			memset(word, 0, WordSize);
			word = strcat(word, WordString+i);
			*Words[WordCount] = word;
			WordCount++;
			WordString[i-1] = '\0';
			lengthOfString = strlen(WordString);
			i--;
			
		}
		i--;
	}
	return STATUS_SUCCESS;
}

/*STATUS WordsToString(char *** Words,int * NumberOfWords,int lengthOfString)
{
	char * retString = NULL;

	retString = (char*)malloc(lengthOfString + 1);
	if (NULL == retString)
	{
		return NULL;
	}
	memset(retString, 0, lengthOfString + 1);
	*NumberOfWords--;
	while (*NumberOfWords > -1)
	{
		memcpy(retString + strlen(retString), Words[*NumberOfWords], strlen(Words[*NumberOfWords]));
		if (*NumberOfWords != 0)
		{
		retString[strlen(retString)] = ' ';
		}
		*NumberOfWords--;
	}

	return retString;
}*/

void BubbleSort(char ** Words, int NumberOfWords)
{
	char * Tmp = NULL;
	char CheckerChar = '\0';
	char CurrentChar = '\0';
	int i = 1;
	int succses = 0;
	int Check = 0;
	unsigned int charCheck = 0;
	while (succses != NumberOfWords - 1)
	{
		CheckerChar = Words[Check][charCheck];
		CurrentChar = Words[i][charCheck];
		if (CheckerChar< CurrentChar)
		{
			Check = i;
			i++;
			succses ++;
			charCheck = 0;
		}
		else if (CheckerChar > CurrentChar)
		{
			Tmp = Words[Check];
			Words[Check] = Words[i];
			Words[i] = Tmp;
			Check = i;
			i++;
			succses = 0;
			charCheck = 0;
		}
		else if(CheckerChar == CurrentChar)
		{
			if ((charCheck < strlen(Words[Check]) - 1) && (charCheck < strlen(Words[i]) - 1))
			{
				charCheck++;
			}
			else if ((charCheck == strlen(Words[Check]) - 1) && (charCheck == strlen(Words[i]) - 1))
			{
				Check = i;
				i++;
				charCheck = 0;
			}
			else if (charCheck == strlen(Words[Check])-1)
			{
				Check = i;
				i++;
				succses++;
				charCheck = 0;
			}
			else if (charCheck == strlen(Words[i]) - 1)
			{
				Tmp = Words[Check];
				Words[Check] = Words[i];
				Words[i] = Tmp;
				Check = i;
				i++;
				succses = 0;
				charCheck = 0;
			}
		}
		if (NumberOfWords == i)
		{
			i = 1;
			Check = 0;
		}
	}
}

void SortWords(char* string)
{
	int *NumberOfWords = NULL;
	char *** WordsPointer = NULL;

	StringToWords(string, NumberOfWords,WordsPointer);
	LengthOfString(*WordsPointer, *NumberOfWords);
	if (NULL == WordsPointer)
	{
		goto FREE;
	}
	
	//BubbleSort(WordsPointer, NumberOfWords);
	//string = WordsToString(WordsPointer, NumberOfWords,lengthOfString);
	if (NULL == string)
	{
		goto FREE;
	}

FREE:
	free(WordsPointer);
}

int main(void)
{
	char * string = NULL;
	int x = 0;

	string = (char*)malloc(strlen("Hello Hey How Hey") + 1);
	memset(string, 0, strlen(string) + 1);
	memcpy(string, "Hello Hey How Hey", strlen("Hello Hey How Hey"));
	SortWords(string);

	return 0;
}