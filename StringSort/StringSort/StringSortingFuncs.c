#include <string.h>
#include <stdlib.h>
#include "StringSortingFuncs.h"
#include "common.h"

// Function that Get pointer to char and return how many words
int HowManyWords(const char * WordString)
{
	int CountWords = 0;
	unsigned int  i = 0;

	while (i < strlen(WordString))
	{

		// A word is found by a space between 
		if (' ' == WordString[i])
		{
			// Add one to word
			CountWords++;
		}

		i++;

	}

	// Add one because that last one doesn't count in the while
	CountWords++;
	return CountWords;
}

//Freeing pointer array pointers
void freeWord(char ** words, int numberOfWords)
{

	for (int i = 0; i < numberOfWords; i++)
	{

		free(words[i]);
		words[i] = NULL;

	}

}

//Getting a length of string that would create by the char pointer array
int LengthOfString(const char** const Words, int numberOfWords)
{
	int retCount = 0;

	retCount += numberOfWords - 1;
	for (int i = 0; i < numberOfWords; i++)
	{
		retCount += strlen(Words[i]);
	}

	return retCount;
}

// A Func that gets a string and making to pointer array
STATUS StringToWords(const char * const WordString, int *const NumberOfWords, char *** const OnlyWords)
{
	char ** Words = NULL;
	char *  word = NULL;
	int WordSize = 0;
	int  i = 0;
	int lengthOfString = 0;
	int WordCount = 0;
	STATUS RetStatus = STATUS_ERROR;

	//setting up the number of Words And saving it for other func
	*NumberOfWords = HowManyWords(WordString);

	//setting up the number of chars in the string
	lengthOfString = strlen(WordString);

	// Alloctaing memory to Words
	Words = (char**)malloc(*NumberOfWords * sizeof(char*));
	//If failed to allocate memory
	if (NULL == Words)
	{
		//Setting up the Reason to failure 
		RetStatus = STATUS_STRING_TO_WORDS_MALLOC1_FAILED;
		goto CleanUp;

	}
	// Setting all the values to 0
	memset((char**)Words, 0, *NumberOfWords * sizeof(char*));

	// I is the index of the array he goes on every char and setting up where is a word
	i = 0;

	while (i < lengthOfString + 1)
	{

		// Space is the indictor for a Word and i==lengthOfString is because the last word doesn't have a space
		if ((' ' == WordString[i]) || (lengthOfString == i))
		{

			//func that get two buffers ,start and length and can copy from one buffer to other
			RetStatus = DuplicateBuffer(WordString, i, WordSize, &word);
			//Checking if the Func Succeed
			if (STATUS_SUCCESS != RetStatus)
			{
				goto CleanUp;
			}

			//Saving the pointer to the word
			Words[WordCount] = word;

			//reseting the WordSize
			WordSize = 0;

			//Add word to the WordCount
			WordCount++;

		}

		WordSize++;
		i++;
	}

	//Saving the Array of pointers
	*OnlyWords = Words;
	//Set Status to Seccess Because we finish the func
	RetStatus = STATUS_SUCCESS;
	//Reseting the pointers
	Words = NULL;
	word = NULL;

CleanUp:
	if (NULL != Words)
	{
		//Function the free all the pointers in the pointer array And setting them to null
		freeWord((char**)Words, *NumberOfWords);
		//Freeing The pointer to the array
		FREEVAR(Words);

	}
	if (NULL != word)
	{
		//Free The pointer to the array
		FREEVAR(word);
	}
	return RetStatus;
}

//A Func that gets a pointer array and making it to string
STATUS WordsToString(const char ** const Words, int NumberOfWords, char** const String)
{
	char * retString = NULL;
	int lengthOfString = 0;
	STATUS retStatus = STATUS_ERROR;

	//Getting the length of the string
	lengthOfString = LengthOfString(Words, NumberOfWords);

	//Alloctaing memory to retString 
	retString = (char*)malloc(lengthOfString + 1);
	//Checking if malloc Failed
	if (NULL == retString)
	{
		//Setting the ret status to Failure
		retStatus = STATUS_WORDS_TO_STRING_MALLOC1_FAILED;
		goto CleanUp;
	}
	//Setting retString to zero
	memset(retString, 0, lengthOfString + 1);

	//Because we start counting array in 0 and not in 1
	NumberOfWords--;

	while (NumberOfWords > -1)
	{
		//Coping the buffer to the end of retString
		memcpy(retString + strlen(retString), Words[NumberOfWords], strlen(Words[NumberOfWords]));

		if (NumberOfWords != 0)
		{
			//Adding Space
			retString[strlen(retString)] = ' ';
		}

		NumberOfWords--;
	}

	//Saving retString after the func
	*String = retString;
	retString = NULL;

	//Saving function to success
	retStatus = STATUS_SUCCESS;

CleanUp:
	if (retString != NULL)
	{
		FREEVAR(retString);
	}
	return retStatus;
}

//A Func that sort a string
STATUS SortWords(char * const string)
{
	int NumberOfWords = 0;
	char ** WordsPointer = NULL;
	STATUS RetStatus = STATUS_ERROR;
	char * tmpString = NULL;

	//Creating A pointer array of all the Words
	RetStatus = StringToWords(string, &NumberOfWords, &WordsPointer);
	//Check if The function failed
	if (STATUS_SUCCESS != RetStatus)
	{
		goto FREE;
	}

	//Sorting the Pointer array
	BubbleSort(WordsPointer, NumberOfWords);

	//Making the pointer array a char array again
	RetStatus = WordsToString(WordsPointer, NumberOfWords, &tmpString);
	//Check if the function failed
	if (STATUS_SUCCESS != RetStatus)
	{
		goto FREE;
	}

	//saving the Function as success
	RetStatus = STATUS_SUCCESS;
	//Deleting the old string
	memset((char*)string, 0, strlen(string) + 1);
	//copy the sorted string
	memcpy(string, tmpString, strlen(tmpString));

FREE:
	if (NULL != WordsPointer)
	{
		FREEVAR(WordsPointer);
	}
	if (NULL != tmpString)
	{
		FREEVAR(tmpString);
	}
	return RetStatus;
}
