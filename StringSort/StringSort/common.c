#include "common.h"
#include <string.h>
#include <stdlib.h>

STATUS TwoStringsToOne(const char * src1, const char * src2, char ** dst)
{
	STATUS retStatus = STATUS_ERROR;
	int tmpLength = 0;
	char * tmpDst = NULL;

	//the length of the new string
	tmpLength = strlen(src1) + strlen(src2)+1;

	//Allocating memory to the string
	tmpDst = (char*)malloc(tmpLength * sizeof(char));
	//Checking if failed
	if (NULL == tmpDst)
	{
		retStatus = STATUS_TWO_STRINGS_TO_ONE_MALLOC1_FAILED;
		goto FREE;
	}
	//Reseting the memory
	memset(tmpDst, 0, strlen(tmpDst));

	//Copy src1 to the string
	memcpy(tmpDst, src1, strlen(src1));
	//Copy src2 to the string
	memcpy(tmpDst + strlen(src1), src2, strlen(src2));

	//Setting the pointer to the pointer value
	*dst = tmpDst;
	tmpDst = NULL;

	//Setting the return status to success
	retStatus = STATUS_SUCCESS;


FREE:
	//Free tmpDst
	FREEVAR(tmpDst);
	

	return retStatus;
}
STATUS DuplicateBuffer(const char * string, int Start, int length, const char ** BufferPointer)
{
	char * BufferPointertmp = NULL;
	STATUS retStatus = STATUS_ERROR;

	//Allocating memory to temporary Buffer 
	BufferPointertmp = (char*)malloc((length + 1) * sizeof(char));
	//Checking if Failed
	if (NULL == BufferPointertmp)
	{
		retStatus = STATUS_DUPLICATE_BUFFER_MALLOC1_FAILED;
		goto CleanUp;
	}
	//Reseting him to 0
	memset(BufferPointertmp, 0, (length + 1) * sizeof(char));

	//Copy if its the start of the buffer
	if (Start - length == 0)
	{
		memcpy(BufferPointertmp, string, length);
	}
	else
	{
		memcpy(BufferPointertmp, string + (Start - length + 1), length);
	}

	//Setting the pointer to the pointer value
	*BufferPointer = BufferPointertmp;
	BufferPointertmp = NULL;

	//Setting the Func to success
	retStatus = STATUS_SUCCESS;

CleanUp:

	if (NULL != BufferPointertmp)
	{
		FREEVAR(BufferPointertmp);
	}

	return retStatus;
}
void BubbleSort(const char **const Words, int NumberOfWords)
{
	char * Tmp = NULL;
	char CheckerChar = '\0';
	char CurrentChar = '\0';
	int i = 1;
	int succses = 0;
	int Check = 0;
	unsigned int charCheck = 0;

	//Check if we passed all the string without switching places
	while (succses != NumberOfWords - 1)
	{

		//The Chars that we check
		CheckerChar = Words[Check][charCheck];
		CurrentChar = Words[i][charCheck];

		//When its correct 
		if (CheckerChar< CurrentChar)
		{
			Check = i;
			i++;
			succses++;
			charCheck = 0;
		}

		//when its it need to be switched
		else if (CheckerChar > CurrentChar)
		{
			Tmp = (char*)Words[Check];
			Words[Check] = Words[i];
			Words[i] = Tmp;
			Check = i;
			i++;
			succses = 0;
			charCheck = 0;
		}

		//If they are the same
		else if (CheckerChar == CurrentChar)
		{

			//Check if we didn't got to the full length of the char array
			if ((charCheck < strlen(Words[Check]) - 1) && (charCheck < strlen(Words[i]) - 1))
			{
				charCheck++;
			}

			//Check if we didn't got to the full length of the char array
			else if ((charCheck == strlen(Words[Check]) - 1) && (charCheck == strlen(Words[i]) - 1))
			{
				Check = i;
				i++;
				charCheck = 0;
			}

			//If we got to the end of the first char array
			else if (charCheck == strlen(Words[Check]) - 1)
			{
				Check = i;
				i++;
				succses++;
				charCheck = 0;
			}

			//If we got to the end of the second char array 
			else if (charCheck == strlen(Words[i]) - 1)
			{
				Tmp = (char*)Words[Check];
				Words[Check] = Words[i];
				Words[i] = Tmp;
				Check = i;
				i++;
				succses = 0;
				charCheck = 0;
			}

		}

		//If we got to the end of the pointer array
		if (NumberOfWords == i)
		{
			i = 1;
			Check = 0;
		}

	}
}
STATUS replace(char * stringSrc,char ** StringDst)
{
	unsigned int i = 0;
	int count = 0;
	char * NewString = NULL;
	STATUS retStatus = STATUS_ERROR;
	char * tmp =NULL ;
	
	tmp = (char*)malloc(sizeof(char) * 2);
	if (NULL == tmp)
	{
		goto FREE;
	}
	memset(tmp, 0, strlen(tmp));
	while (i < strlen(stringSrc))
	{
		*tmp = stringSrc[i];
		if (memcmp(tmp,"<",sizeof("<"))){
			count++;
		}
		i++;
	}

	NewString = (char*)malloc(sizeof(char) * (strlen(stringSrc) - count));
	if (NULL == NewString){
		retStatus = STATUS_REPLAE_MALLOC1_FAILED;
		goto FREE;
	}
	memset(NewString, 0, strlen(NewString));

	i = 0;
	while (i < strlen(stringSrc))
	{
		if (strcmp(&(stringSrc[i]), "\n")){
		}
		else {
			memcpy(NewString, stringSrc, sizeof(char));
		}
		i++;
	}

	*StringDst = NewString;
	NewString = NULL;
	retStatus = STATUS_SUCCESS;
FREE:
	FREEVAR(NewString);
	return retStatus;
}