/*
Bind socket to port 8888 on localhost
*/
#include "\Git\training\StringSort\StringSort\common.h"
#include<stdio.h>
#include<winsock2.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

STATUS SetMSG(const char * const dir,char ** MSG)
{
	FILE *fp = { 0 };
	char * HTML = NULL;
	int fileLength = 0;
	char *reply = NULL;
	STATUS retStatus = STATUS_ERROR;
	int tmp = 0;

	fp = fopen(dir, "r");

	fseek(fp, 0, SEEK_END);
	fileLength = ftell(fp);
	rewind(fp);

	HTML = (char*)malloc((fileLength+1) * sizeof(char));
	if (NULL == HTML)
	{
		retStatus = STATUS_SET_MSG_MALLOC1_FAILED;
		goto FREE;
	}
	memset(HTML, 0, (fileLength + 1) * sizeof(char));
	
	fread(HTML, fileLength, 1, fp);

	reply = (char*)malloc((121+ strlen(HTML)+1)*sizeof(char));
	if (NULL == reply)
	{
		retStatus = STATUS_SET_MSG_MALLOC2_FAILED;
		goto FREE;
	}
	memset(reply, 0, (121 + strlen(HTML) + 1) * sizeof(char));

		sprintf(reply, "HTTP/1.0 200 OK\n" \
		"Server: Apache/2.2.3\n" \
		"Content-Type: text/html\n" \
		"Content-Length: %d\n" \
		"Accept-Ranges: bytes\n" \
		"Connection: close\n" \
		"\n" \
		"%s\n", fileLength, HTML);

	*MSG = reply;
	reply = NULL;
	fclose(fp);
	
	retStatus = STATUS_SUCCESS;

FREE:
	FREEVAR(reply);
	FREEVAR(HTML);
	return retStatus;
}
STATUS StartHTTPServer()
{

	WSADATA wsa = { 0 };
	SOCKET s = { 0 };
	SOCKET new_socket = { 0 };
	struct sockaddr_in server = { 0 };
	struct sockaddr_in client = { 0 };
	int c = 0;
	int recv_size = 0;
	char * server_reply = NULL;
	char * MSG = NULL;
	char * dir = "C:\\tmp\\html.txt";
	STATUS retStatus = STATUS_ERROR;

	SetMSG(dir, &MSG);
	server_reply = (char*)malloc(4000 * sizeof(char));
	if (NULL == server_reply)
	{
		retStatus = STATUS_START_HTTP_SERVER_MALLOC1_FAILED;
		goto FREE;
	}
	memset(server_reply, 0, 4000 * sizeof(char));



	printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		goto FREE;
	}

	printf("Initialised.\n");

	//Create a socket
	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s == INVALID_SOCKET)
	{
		printf("Could not create socket : %d", WSAGetLastError());
		goto FREE;
	}

	printf("Socket created.\n");

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(1212);

	//Bind
	bind(s, (struct sockaddr *)&server, sizeof(server));
	if (s == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d", WSAGetLastError());
		goto FREE;
	}

	printf("Bind done");


	//Listen to incoming connections
	listen(s, 3);

	//Accept and incoming connection
	printf("Waiting for incoming connections...");
	
	c = sizeof(struct sockaddr_in);
	new_socket = accept(s, (struct sockaddr *)&client, &c);
	if (new_socket == INVALID_SOCKET)
	{
		printf("accept failed with error code : %d", WSAGetLastError());
		goto FREE;
	}

	printf("Connection accepted\n");
	recv_size = recv(new_socket, server_reply, 4000, 0);
	if (recv_size < 0)
	{
		puts("recv failed");
		goto FREE;
	}
	else {
		printf(server_reply);
		if (memcmp(server_reply, "GET", 3) == 0)
		{
			send(new_socket, MSG, strlen(MSG), 0);
		}
	}


FREE:
	closesocket(s);
	WSACleanup();
	FREEVAR(server_reply);
	FREEVAR(MSG);
	return retStatus;
}

int main(void)
{
	StartHTTPServer();
	return 0;
}
