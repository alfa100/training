#include "LinkedList.h"
#define Main
#include <stdio.h>
int main(void)
{
	List* list = LIST_Create();
	LIST_AddToBeginning(list, 8);
	LIST_Print(list);
	printf("\n");

	LIST_AddToBeginning(list, 9);
	LIST_Print(list);
	printf("\n");
	LIST_DeleteNode(list, list->root->NextNode);
	LIST_Print(list);
	printf("\n");

	printf("%d\n",LIST_GetLength(list));
	LIST_Print(list);
	printf("\n");
	LIST_AddToBeginning(list, 9);
	LIST_Print(list);
	printf("\n");
	printf("%d\n", LIST_GetLength(list));

	LIST_AddToBeginning(list, 6);
	LIST_Print(list);
	printf("\n");
	printf("%d",LIST_GetFirstNode(list)->value);

	getchar();
	return 0;
}