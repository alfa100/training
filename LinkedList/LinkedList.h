#pragma once

typedef struct NodeT
{
	int value;
	struct NodeT *NextNode;
}Node;

typedef struct ListT
{
	Node *root;
}List;

List* LIST_Create(void);
int LIST_AddToBeginning(List *list, int num);
int LIST_DeleteNode(List *list,Node *node);
int LIST_GetLength(List *list);
Node* LIST_GetFirstNode(List* list);
void LIST_Free(List* list);
int LIST_Print(List* list);