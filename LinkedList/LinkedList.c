#include <stdlib.h>
#include <stdio.h>
#include "LinkedList.h"

List* LIST_Create(void)
{
	List *ret;
	ret = (List*)malloc(sizeof(List));
	if (NULL == ret)
	{
		return NULL;
	}
	ret->root = (Node*)malloc(sizeof(Node));
	if (NULL == ret->root)
	{
		return NULL;
	}
	ret->root->NextNode = NULL;
	ret->root->value = 0;

	return ret;
}//Create Empety List

int LIST_AddToBeginning(List* list, int num)
{
	Node *New = NULL;

	New = (Node*)malloc(sizeof(Node));

	if (NULL == New)
	{
		return 1;
	}

	New->value = num;
	New->NextNode = list->root;
	list->root = New;

	return 0;
}//Add to node the beginning of the list

int LIST_DeleteNode(List* list,Node* node)
{
	Node *Checker = NULL;
	Node *tmp = NULL;

	tmp = list->root;
	Checker = tmp->NextNode;

	if (node == tmp)
	{
		list->root = Checker;
		return 0;

	}

	while (NULL != Checker)
	{

		if (node == Checker)
		{
			tmp->NextNode = Checker->NextNode;
			return 0;
		}

		tmp = Checker;
		Checker = Checker->NextNode;
	}
	return 1;
}//Delete node from the list

int LIST_GetLength(List* list)
{
	Node *tmp = NULL;
	int count = 0;

	tmp = list->root;

	while (tmp != NULL)
	{
		count++;
		tmp = tmp->NextNode;
	}

	return count;
}//returning the length of the list

Node* LIST_GetFirstNode(List* list)
{
	return list->root;
}//returning the first node of the list

void LIST_Free(List* list)
{
	Node* current = list->root->NextNode;
	while (current != NULL)
	{
		free(list->root);
		list->root = current;
	}
	free(current);
	free(list);
}

int LIST_Print(List* list)
{
	Node* tmp = list->root;
	while (tmp != NULL)
	{
		printf("->");
		printf("%d",tmp->value);
		tmp = tmp->NextNode;
	}
	return 0;
}