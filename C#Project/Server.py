import threading
import socket
import time
import Queue
import thread

def inputMSG(Client,MsgQue,Clients,addr):
    while 1:
        try:
            MsgQue.put(Client.recv(4096))
            # print(    "Input!")  # For Debugging
        except socket.error:  # If the client close the connection
            try:  # Trying to remove the client from list
                Clients.remove(Client)
                print("Client Closed the connection in Input with ")
                print(addr)
                thread.exit()
            except ValueError:  # When the removing failed
                print("Client Closed the connection in Input with ")
                print(addr)
                thread.exit()


# Doing the input of the Chat
def StartClients(Clients, MsgQue, server):
    while 1:
        #print ("Im In input")
        # Accepting new clients and adding them to list of clients
            tmpCli, addr = server.accept()
            print("Connection established with ")
            print(addr)
            Clients.append(tmpCli)
            CliThread = threading.Thread(target=inputMSG, args=(tmpCli, MsgQue,Clients,addr))
            CliThread.start()

            #print("No new Sockets")
            # Taking the input from the Client and adding him(input,client,client name) to queue of messages


# Doing the Output of the Chatff
def outputmsg(Clients, MsgQue):
    while 1:
        #print ("Im In Output")
        # Checking if there is a message waiting to be send
        if not MsgQue:
            pass
        else:
            currentMSG= MsgQue.get(block = True)
            # Outputing to the clients
            for cli in Clients:
                    try:  # Trying to send name and message to client
                        cli.send(currentMSG)
                    except socket.error:  # If the client close the connection
                        try:  # Trying to remove the client from list
                            Clients.remove(cli)
                            print("Client Closed the connection in Output")
                        except ValueError:  # When the removing failed
                            print("Problem removing the client")



def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "0.0.0.0"
    port = 5000

    try:  # Trying to bind the server to this port
        server.bind((host, port))
        print("Server binded to port 5000")
        server.listen(5)
        print("Server is listening")
        Clients = []
        MsgQue = Queue.Queue()
        threads = []

        # Setting and Starting the threads
        output = threading.Thread(target = outputmsg,args = (Clients,MsgQue))
        input = threading.Thread(target = StartClients, args =(Clients, MsgQue, server))
        # Infinity loop so the program will not close
        output.start()
        input.start()
        while 1:
            time.sleep(29030400)
    except socket.error:  # Program already uses this port
        pass
       # print ("Couldn't Bind the server to this address")


if __name__ == "__main__":
    main()